# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repo is for storing open source high speed large data communication framework over PCI. This work will help realizing inter virtual machines shared memory communications.


### How do I get set up? ###

* Use a Linux machine with Ubuntu 16.04 installed. (You may choose other distribution and install packages according to that distribution as well)
* Clone this repo (username@bitbucket.org:visteon-git/project-os-bsp-adas-bsp-framework.git)
* Install qemu-system-x86, qemu-utils on ubuntu host.
* Inside the git repo, create 2 virtual machines, name them as guest1 & guest2 as mentioned in [QEMU Debian Wiki](https://wiki.debian.org/QEMU) page.
  * Note: The VM disk image name shall be guest1.img and guest2.img respectively.
* Install packages such as build-essentials, cmake, vim, etc inside VM guests, so that you can build source code from withing the VM. (I recommend, you create host1 first, install all packages and finally copy and rename host1 as host2.)
* Using the script 'qemu-start' stored in this repo, start both the hosts as below
*  * ./qemu-start guest1
*  * ./qemu-start guest2
* Copy the contents of guest_src-qemu to using scp command
*  * scp -P 2222 -r guest_src-qemu localhost:~
*  * scp -P 2223 -r guest_src-qemu localhost:~
* login to both VMs and do the following
*  * cd guest_src-qemu/kernel_modules
*  * make
*  * cd ~/guest_src-qemu/userspace-apps/
*  * mkdir build && cd build
*  * cmake ..
*  * make

### How to run the sample or test application? ###

* Open terminal and change directory to the clone of the this repository
* Start Inter VM Shared Memory server using the script ./shm-server-start
* Open new tab in terminal and start both VMs as detailed in previous section
* Login to both VMs
* cd guest_src-qemu && ./insmod-uio
* cd userspace-apps/build
* sudo ./getident /dev/uio0 and note down the VM IDs
* From guest1
*  * sudo ./uio_send /dev/uio0 'other VM ID'
*  * Note down the timestamp of the send message.
* From guest2
*  * sudo ./uio_read /dev/uio0 'other VM ID'
*  * You should get a nessage like "uio_send.c:74 --> This message is written to memory region pointed by debian2's PCI BAR2 register. This is written at 10/07/17 - 03:05PM 28 seconds"
  

### Who do I talk to? ###

* Contact Aananth C N (aananth.cn@visteon.com) or Venkatesh Prabhu (vprabhu1@visteon.com)
* OS / BSP Team: Alex Schaefer / Babu Viswanathan