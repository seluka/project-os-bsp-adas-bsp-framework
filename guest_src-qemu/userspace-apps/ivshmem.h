#ifndef IVSHMEM_HDR
#define IVSHMEM_HDR


enum ivshmem_registers {
    IntrMask = 0,
    IntrStatus = 4,
    IVPosition = 8,
    Doorbell = 12
};

int ivshmem_send(void *, int ivshmem_cmd, int destination_vm);
int ivshmem_recv(int fd, void *memptr);
void ivshmem_print_opts(void);

#endif
